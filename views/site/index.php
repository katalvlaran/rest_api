<?php

/* @var $this yii\web\View */

$this->title = 'REST Task';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Welcome!</h1>

        <p class="lead">This place is my task (REST API). You can use several types of queries for different results (or just testing).</p>

    </div>

    <div class="body-content">

        <div class="row">

            <div class="col-lg-4" style="margin-left: 15%">
                <h2>What you can do</h2>
                <ul>
                    <li>GET api/users</li>
                    <li>GET /api/users/search/UserName</li>
                    <li>POST /api/users</li>
                    <li>PUT /api/users/13</li>
                    <li>DELETE api/users/13</li>
                </ul>
            </div>

            <div class="col-lg-6">
                <h2>What you can get</h2>
                <ul>
                    <li style="list-style-type: none;">- returns all users</li>
                    <li style="list-style-type: none;">- searches for a user named UserName</li>
                    <li style="list-style-type: none;">- adds a user</li>
                    <li style="list-style-type: none;">- updates the user with this ID</li>
                    <li style="list-style-type: none;">- delete the user with this ID</li>
                </ul>
            </div>

        </div>

    </div>
</div>

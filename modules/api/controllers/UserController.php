<?php

namespace app\modules\api\controllers;

use app\models\Users;
use yii\rest\ActiveController;

class UserController extends ActiveController
{
    public $modelClass = 'app\models\Users';

    public function actionSearch($name)
    {
        $user = Users::find()->where(['name' => $name]);

        $user = $user->all() != NULL ? $user->all() : "No user`s with name'$name'";

        return $user;
    }
}
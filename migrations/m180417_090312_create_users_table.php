<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m180417_090312_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'phone' => $this->string()->notNull(),
            'phone_2' => $this->string()->null() ,
            'email' => $this->string()->null(),
        ]);

        $this->createIndex(
            'idx-users-name',
            'users',
            'name'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('users');
    }
}

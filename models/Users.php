<?php

namespace app\models;

use Yii;
use yii\helpers\Url;
use yii\web\Link;
use yii\web\Linkable;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string $phone_2
 * @property string $email
 */
class Users extends ActiveRecord implements Linkable
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone'], 'required'],
            [['name', 'phone', 'phone_2', 'email'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'phone' => 'Phone',
            'phone_2' => 'Phone 2',
            'email' => 'Email',
        ];
    }

    public function getLinks()
    {
        return [
            Link::REL_SELF => Url::to(['user/view', 'id' => $this->id], true),
        ];
    }

}

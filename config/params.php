<?php

return [
    'adminEmail' => 'mailto:katalvlaran@gmail.com',
    'adminLinkedIn' => 'https://www.linkedin.com/in/katalvlaran/',
    'adminSkype' => 'skype:katalvlaran?add',
    'adminFacebook' => 'https://www.facebook.com/katalvlaran',
    'applicationName' => 'REST-Task',
];
